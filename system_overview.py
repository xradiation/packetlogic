
# Python 2 script to communicate to packetlogic devices
# and potentially graph the returned information

import packetlogic2 as pl2
from os.path import exists

plIPArray = ['10.44.45.228', '10.44.45.133', '10.44.45.37', '10.44.45.196']
plNewCreds = ['admin', 'password']
plOldCreds = ['admin', 'pldemo00'] 

# Fix this
plIP = "10.44.45.196"
plUser = "admin"
plPasswd = "pldemo01"
# For each IP get the traffic array, and output it
# to a text file to be plotted using python3 <- Fix This
    # Store the firmware version on a file with IP to choose
    # what creds you should use
if os.path.exists(".plDevices") == True:

    for i in range(0, 3):
        plConnObj = pl2.connect(plIPArray[i], plUser, plPasswd)
else:
   # Get the version and addit to the file 


# Todo: check version to determine what to use
# plNewCreds or plOldCreds, possibly check after
# fist manual connection and store the observation
# for auto connection next time; Fall back to manual
# in case of a connection failure in the future

sysOver = plConnObj.SystemOverview()
#print(sysOver.system_list().distversion())


# List systemIDs
print(sysOver.sysinfo_systems_list())
#print(sysOver.system_list())
#print(sysOver.template_list())

# 7 for Ethernet/Ethernet Bytes
trafficArray = sysOver.system_graph(7, input("What system ID: "))
print(trafficArray)
#print(sysOver.system_graph(7, sysOver.sysinfo_systems_list()[1]))

